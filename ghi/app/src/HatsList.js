import { Link } from 'react-router-dom';

function HatsList(props) {
  const deleteHat = async (id) => {
    const url = `http://localhost:8090/api/hats/${id}`

    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok){
      console.log("hat deleted")
      window.location.reload(false);
    }

  }
    return (
      <div>
        <table className = "table table-striped" >
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Location</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
              return (
                <tr key={hat.href}>
                  <td>{hat.fabric}</td>
                  <td>{hat.style_name}</td>
                  <td>{hat.color}</td>
                  <td>
                    <img src= {hat.picture_url} width="100px" height="100px" alt="" />
                  </td>
                  <td>{hat.location}</td>
                  <td>
                    <button className="btn btn-outline-danger" onClick={() => deleteHat(hat.id)}>delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-end">
        <Link to="/hats/new" className="btn btn-outline-info btn-lg px-4 gap-3">Create new hat</Link>
      </div>
    </div>
    )
}

export default HatsList;