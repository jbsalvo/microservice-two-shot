import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatsList from './HatsList';
import MainPage from './MainPage';
import Nav from './Nav';

import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import HatsForm from './HatsForm';


function App(props) {

  if (props.shoes === undefined || props.hats === undefined) {
    return null;
  }


  return (

    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={props.shoes} />} />
            <Route path="new" element={<ShoesForm />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatsList hats={props.hats} />} />
            <Route path="new" element={<HatsForm />} />
          </Route>
          
        </Routes>
      </div>
    </BrowserRouter>

  );
}
export default App;
